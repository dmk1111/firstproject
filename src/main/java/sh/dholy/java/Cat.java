package sh.dholy.java;

public class Cat extends Animal {

    private int clawsOnPaw;
    private String eyeColor;
    private int miceCaoughtAmount;

    public Cat(String name, String breed, String color, int clawsOnPaw, String eyeColor, int miceCaoughtAmount) {
        super(name, breed, color);
        this.clawsOnPaw = clawsOnPaw;
        this.eyeColor = eyeColor;
        this.miceCaoughtAmount = miceCaoughtAmount;
    }

    @Override
    public String sound() {
        return "Meow-meow";
    }

    @Override
    public String toString() {
        return "Cat{" + super.toString() +
                "clawsOnPaw=" + clawsOnPaw +
                ", eyeColor='" + eyeColor + '\'' +
                ", miceCaoughtAmount=" + miceCaoughtAmount +
                '}';
    }
}
