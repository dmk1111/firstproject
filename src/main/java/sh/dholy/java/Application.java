package sh.dholy.java;

public class Application {

    public static void main(String[] args) {
        Animal myDog = new Dog("Barbos", "Buldog", "green", 4);
//        System.out.println(myDog);

        Animal myCat = new Cat("Murchyk", "fold", "gray", 5, "green", 15);
//        System.out.println(myCat);

        print(myDog);
        print(myCat);
    }

    public static void print(Animal animal) {
        System.out.println(animal);
        System.out.println(animal.sound());
    }
}
